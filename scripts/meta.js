'use strict';
const { execSync } = require('child_process');
const fs = require('fs');
const crypto = require('crypto');
const os = require('os');

// checksum bundles
const summers = [];
const checksums = {};
const buildDir = `${ __dirname }/../build`;
fs.readdirSync(buildDir)
	.filter((file) => fs.statSync(`${ buildDir }/${ file }`).isDirectory())
	.forEach((platform) =>
		fs.readdirSync(`${ buildDir }/${ platform }`)
			.filter((file) => /^hub-service-node/.test(file))
			.forEach((file) => summers.push(new Promise((resolve) => {
				const sum = crypto.createHash('sha256');
				const fullPath = `${ buildDir }/${ platform }/${ file }`;
				fs.ReadStream(fullPath)
					.on('data', (data) => sum.update(data))
					.on('end', () => {
						checksums[`${ platform }/${ file }`] = {
							'sha256': sum.digest('hex'),
							'ctime': fs.statSync(fullPath).ctime
						};
						resolve();
					})
			})))
	);

// output checksums when they're ready, along with some other potentially relevant debugging info
Promise.all(summers).then(() => {
	console.log(JSON.stringify({
		checksums,
		'agent': execSync(`grep -e VERSION\\\\s\\\\+= ${ __dirname }/../server.js`, { 'encoding': 'utf8' }).replace(/^.*?'|'(.|[\r\n])*/g, ''),
		'node': process.versions.node,
		'builder': `${ os.hostname } ${ process.platform } ${ process.arch } ${ os.release }`
//		'config': process.config.variables // probably not necessary
	}, null, '\t'));
});

