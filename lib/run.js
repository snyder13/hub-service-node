'use strict';
const { exec } = require('child_process');

/// @TODO other invocation methods & response formats
const t = module.exports = {
	'positional': (val) => JSON.stringify(val),
	'named':  (name, val) => `--${ name }=${ JSON.stringify(val) }`,
	'run': (cmd, args) => new Promise((resolve, reject) => {
		exec(`${ cmd } ${ args.join(' ') }`, (err, stdout, stderr) => {
			if (err) {
				return reject(err);
			}
			resolve({ stdout, stderr });
		});
	}),
	'exitDescription': (err) => `exit code ${ err.code }${ err.killed ? ' (killed)' : '' }`
};
