'use strict';
const http = require('http');
const fs = require('fs');
const statusCode = process.argv[2] ? process.argv[2] : 200;

const index = fs.readFileSync('./index.html', { 'encoding': 'utf8' });
const server = http.createServer((req, res) => {
	res.writeHead(statusCode, { 'Content-Type': 'text/html' });
	res.write(index.replace('{{headers}}', JSON.stringify(req.headers, '', '\n')));
	res.end();
});
server.listen(8080);
