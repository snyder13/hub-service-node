'use strict';
const VERSION = '0.0.1';

const fs = require('fs'); // persist services.json
const conf = JSON.parse(fs.readFileSync('./conf.json')); // NB: do not change to infer file type, messes up nexe bundling
const crypto = require('crypto');
const os = require('os'); // basic OS info for interface labeling
const WebSocket = require('ws'); // communication w/ server
const { checkServerIdentity } = require('tls'); // used in custom cert validation
const msgpack = require('msgpack5')(); // transport format for websocket messages
const debug = require('debug')('agent'); // debug messages, run with DEBUG=agent (or DEBUG=*, potentially bringing in debug from client libraries) to see

const actions = require('./actions'); // websocket messages are delegated to these files based on their 'action' property

const sock = new WebSocket(
	`${ conf['url-base'] }/connect`,
	undefined, // subprotocols, use default
	{          // options, both for 'ws' and underlying 'https.request()'

		// verify pinned cert. connecting to a masquerading host is dangerous due to data service `exec`-ing
		'checkServerIdentity': (host, cert) => {
			// do base verification -- trusted authority, expiration, etc
			const err = checkServerIdentity(host, cert);
			if (err) {
				return err;
			}
			// match shipped fingerprint
			if (crypto.createHash('sha256').update(cert.pubkey).digest('base64') !== conf['pin-key']) {
				return `Certificate verification error: ${ cert.subject.CN } does not match pinned key fingerprint`;
			}
		}
	}
);

sock.on('open', (err) => {
	if (err) {
		throw err;
	}

	const ctx = {
		conf,
		debug,
		// NB: don't require() here, that's redirected to look inside the nexe bundle
		'services': fs.existsSync('./services.json') ? JSON.parse(fs.readFileSync('./services.json')) : {}
	};

	// kick off initial connection. action handlers should keep things afloat after
	const hello = {
		'action': 'hello',
		'services': ctx.services,
		'version': VERSION,
		'hostname': os.hostname(),
		'os': {
			'platform': os.platform(),
			'release': os.release()
		}
	};
	debug('>', hello);
	sock.send(msgpack.encode(hello));

	sock.on('message', (msg) => {
		try {
			msg = msgpack.decode(msg);
		}
		catch (ex) {
			console.error('malformed message', ex);
			return;
		}
		debug('<', msg);

		// dispatch to handlers
		if (actions[msg.action]) {
			const action = msg.action;
			delete msg.action;
			actions[action](msg, (res) => {
				debug('>', msg);
				sock.send(msgpack.encode(res));
			}, ctx);
		}
		else {
			console.error('unhandled action', msg.action);
		}
	});
});

