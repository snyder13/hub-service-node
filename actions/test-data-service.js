'use strict';
const { positional, run, exitDescription } = require('../lib/run');
const prettyBytes = require('pretty-bytes');

module.exports = (msg, reply, ctx) => {
	const msgBase = {
		'id': ctx.id,
		'action': 'dataServiceStatus',
		'definition': msg.definition,
		'name': msg.name,
		'command': msg.command,
		'refId': msg.refId
	};
	console.log('testing', msg.name, msg.command);
	const hasAllValues = msg.definition.inputs.length === 0 || msg.definition.inputs.every((inp) => inp.presets && inp.presets.some((preset) => preset.default));
	if (hasAllValues) {
		const defaults = [];
		msg.definition.inputs.forEach((inp) => {
			inp.presets.forEach((pre) => {
				if (pre.default) {
					defaults.push(positional(pre.value));
				}
			});
		});
		run(msg.command, defaults)
			.then(({ stdout, stderr }) => {
				if (stderr) {
					console.error(stderr);
					msgBase.error = `stderr: ${ stderr }`;
				}
				console.log(`\toutput size was ${ prettyBytes(stdout.length) }`);
				msgBase.output = stdout;
				reply(msgBase);
			})
			.catch((err) => {
				console.error(err);
				msgBase.error = exitDescription(err);
				reply(msgBase);
			});
	}
	else {
		reply(msgBase);
	}
}
