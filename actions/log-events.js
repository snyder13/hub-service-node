'use strict';
module.exports = (msg) => {
	Object.values(msg.events).forEach(({ message, context, timestamp, type, url }) =>
		console.log(`${ timestamp } ${ url } - ${ type } event: ${ message } ${ context ? JSON.stringify(context) : '' }`)
	);
}
