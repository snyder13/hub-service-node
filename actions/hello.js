'use strict';
const qrcode = require('qrcode-terminal');

/**
 * Set the session id provided by the server in context, output manage url
 *
 * @TODO respond to update prompts, since we just sent our version
 */
module.exports = (msg, _reply, ctx) => {
	ctx.id = msg.sid;
	// add id (with dashes for easier typing) to url format
	const manageUrl = `${ ctx.conf['url-base'] }/${ ctx.id.replace(/(.{4})(?!$)/g, '$1-') }`;
	console.log(`Connected, visit ${ manageUrl } to manage`);
	qrcode.generate(manageUrl);
}
