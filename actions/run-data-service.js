'use strict';
const { positional, run, exitDescription } = require('../lib/run');
const prettyBytes = require('pretty-bytes');

module.exports = (msg, reply, ctx) => {
	console.log(msg.refId, msg.name, msg.command, msg.args);
	const msgBase = {
		'id': ctx.id,
		'action': 'dataServiceResponse',
		'refId': msg.refId
	};

	run(msg.command, msg.args.map(positional))
		.then((stdout, stderr) => {
			console.log(`\toutput size was ${ prettyBytes(stdout.length) }`);
			msgBase.error = stderr;
			msgBase.result = stdout;
			reply(msgBase);
		})
		.catch((err) => {
			console.error(err);
			msgBase.error = exitDescription(err);
			reply(msgBase);
		});
}
