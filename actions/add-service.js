'use strict';
const fs = require('fs');

/**
 * Store service definitions the server has decided are potentially valid
 *
 * Currently this only happens when the user clicks the "Save & Test" button in
 * the config interface.
 */
let backedUp = false;
module.exports = (msg, _reply, ctx) => {
	ctx.services[msg.name] = msg;
	console.log('registered service', msg);
	const write = () => fs.writeFileSync('./services.json', JSON.stringify(ctx.services, null, '\t'));
	if (!backedUp) {
		if (fs.existsSync('./services.json')) {
			console.log('\tcreating services.json.bak, writing new registration(s) to services.json');
			fs.createReadStream('./services.json').pipe(fs.createWriteStream('./services.json.bak').on('done', write));
		}
		backedUp = true;
	}
	else {
		write();
	}
};
