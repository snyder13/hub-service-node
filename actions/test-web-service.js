'use strict';
const request = require('request-promise');

/**
 * Configuration verification for a web service.
 *
 * Non-200 status is often intentional and is thus treated as a warning, only
 * network exceptions return errors
 */
module.exports = (msg, reply) => {
	const baseMsg = {
		'action': 'webServiceStatus',
		'name': msg.name,
		'service-url': msg['service-url'],
		'refId': msg.refId
	};
	console.log('testing', msg.name, msg['service-url']);
	request({
		'uri': msg['service-url'],
		'method': 'GET',
		'resolveWithFullResponse': true
	})
		.then((res) => {
			baseMsg.status = res.statusCode;
			console.log(`\t${ res.statusCode }`);
			reply(baseMsg);
		})
		.catch((err) => {
			if (err.name === 'StatusCodeError') {
				baseMsg.status = err.response.statusCode;
				console.warn(`\t${ err.response.statusCode }`);
			}
			else {
				baseMsg.error = err.toString();
				console.error(`\t${ err.toString() }`);
			}
			reply(baseMsg);
		});
}
