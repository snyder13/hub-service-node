'use strict';
const request = require('request-promise');
const prettyBytes = require('pretty-bytes');

module.exports = (msg, reply, ctx) => {
	function sendBack(res) {
		console.log(`${ msg.refId } ${ res.statusCode } ${ res.headers['content-type'] ? res.headers['content-type'] : 'unknown' } ${ prettyBytes(res.headers['content-length'] ? res.headers['content-length'] : res.body.length).replace(/\s+/, '') }`);
		reply({
			'action': 'webResponse',
			'refId': msg.refId,
			'response': {
				'statusCode': res.statusCode,
				'headers': res.headers,
				'body': res.body
			}
		});
	}
	console.log(`${ (new Date).toISOString() } ${ msg.request.method } ${ msg.request.path } ${ msg.refId }`);
	request({
		'uri': ctx.services[msg.service]['service-url'] + msg.request.path,
		'method': msg.request.method,
		'headers': msg.request.headers,
		'resolveWithFullResponse': true
	})
		.then(sendBack)
		.catch((err) =>
			sendBack(err.response ? err.response : { 'headers': {}, 'body': err.toString(), 'status': 500 })
		)
};
