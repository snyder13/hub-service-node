/**
 * usage: node fermi-named-progress [--progress] --T $T --Ef $eF
 * suggestions from tool.xml:
 * 	0 < T < 500 -- 300 (room temp, default), 77 (liquid nitrogren), or 4.2 (liquid helium)
 * 	-10 < Ef < 10 -- 0 (default)
 */
const argv = require('minimist')(process.argv.slice(2));
const T    = argv.K; // K
const Ef   = argv.Ef; /// eV
const kT   = 8.61734e-5 * T;
const Emin = Ef - 10*kT;
const Emax = Ef + 10*kT;
const dE   = 0.005*(Emax - Emin);
let E    = Emin;
let f    = 0.0;

while (E < Emax) {
	f = 1.0/(1.0 + Math.exp((E - Ef)/kT));
	console.log({ f, E });
	if (argv.progress) {
		console.log({ 'progress': (E-Emin)/(Emax-Emin)*100 });
	}
	E = E + dE;
}

