/**
 * usage: node fermi-positional $T $Ef
 * suggestions from tool.xml:
 * 	0 < T < 500 -- 300 (room temp, default), 77 (liquid nitrogren), or 4.2 (liquid helium)
 * 	-10 < Ef < 10 -- 0 (default)
 */

const T    = process.argv[2]; // K
const Ef   = process.argv[3]; /// eV
const kT   = 8.61734e-5 * T;
const Emin = Ef - 10*kT;
const Emax = Ef + 10*kT;
const dE   = 0.005*(Emax - Emin);
let E    = Emin;
let f    = 0.0;

while (E < Emax) {
	f = 1.0/(1.0 + Math.exp((E - Ef)/kT));
	console.log(`${ f } ${ E }`);
	E = E + dE;
}

