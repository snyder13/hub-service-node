# hub-service-node
This is a downloadable agent that proxies web services and CLI scripts to publish their interfaces on the web.

This service makes a websocket connection to the entity identified in `conf.js`. If you change this URL you should change the accompanying SHA-256 signature to match the host's SSL cert.

The server that runs at this URL is in this repo: (@TODO)

After sending a connection message to the server, the node is issued a session id which it uses to generate a link to open a configuration interface for the node.

The interface is included in hub-polymer: https://bitbucket.org/snyder13/hub-polymer/src/master/src/components/agent/

Thereafter, the server generates websocket events in response to changes to the configuration and to people invoking services defined with it. The handlers for these events are in the `actions/` folder.


This is currently in the proof-of-concept stage and not practically useful for anything.


## Running
Builds are included in the `build/` path.

To run the source, in case you want to make changes, install a recent version of node, `npm install -g yarn`, and:

	$ yarn install
	$ yarn start

Either using the pre-built binaries or yarn, you can prefix the start command with `DEBUG=*` to print additional information including all the messages that come and go over the wire.

## Building
	$ yarn install
	$ yarn build
